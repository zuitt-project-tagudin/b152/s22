

create sample docs following the models for course booking system

user1 and user2
course1 cours2

user1 is enrolled in both
user2 is enrolled in course2


users
		{
			(id) - unique for document entry
			username
			firstname/lastname
			email
			password
			isAdmin
			contact
			enrollmentDetails: [
				{
					id,
					user_id,
					dateEnrolled,
					status
				}
			]
		}

course
	{
		id,
		name,
		description,
		price,
		enrollmentDetails: [
			{
				id,
				user_id,
				dateEnrolled,
				status
			}
		]

	}